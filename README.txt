
Role Weight
-----------

To install, place the entire roleweight forlder into you modules directory.
Go to Administer -> Site building -> Modules and enable Role Weight module.

Usage
-----

The Role Weight module allows you to set roles ordering. This is very useful in situations, when need to know which rule is more important than the other.

The module gives no functionality to end user, but it's effect can be used by other modules (e.g. Form Adjust).

Go to the Administer -> User management -> Role weight to set roles ordering. Do that by dragging the roles. Click 'save' when you've done.

Maintainers
-----------
Adrian Pitwor